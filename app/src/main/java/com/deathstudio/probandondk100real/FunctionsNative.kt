package com.deathstudio.probandondk100real

import android.content.Context
import android.graphics.Bitmap

class FunctionsNative {



    companion object{

        external fun generarToast(context: Context,mensaje: String,duration: Int)
        external fun stringFromJNI(): String
        external fun holi() : String

        external fun convertToGray(bitmapIn: Bitmap, bitmapOut: Bitmap)
        external fun findEdges(bitmapIn: Bitmap, bitmapOut: Bitmap)

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }

    }
}