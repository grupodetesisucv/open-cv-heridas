package com.deathstudio.probandondk100real

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import kotlinx.android.synthetic.main.activity_main.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.deathstudio.probandondk100real.FunctionsNative.Companion.convertToGray
import com.deathstudio.probandondk100real.FunctionsNative.Companion.findEdges
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.imgproc.Imgproc.*
import java.util.ArrayList
import org.opencv.core.Scalar
import org.opencv.core.MatOfPoint
import org.opencv.core.Mat
import org.opencv.core.Core
import org.opencv.core.CvType
import java.math.RoundingMode
import java.text.DecimalFormat


class MainActivity : AppCompatActivity() {

    lateinit var bitmapOrig : Bitmap
    lateinit var bitmapGray : Bitmap
    lateinit var bitmapWip : Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //generarToast(this,"Toast con NDK",Toast.LENGTH_LONG)
        //Toast.makeText(this,"jdnjsad",Toast.LENGTH_LONG).show()

        val options = BitmapFactory.Options()
        // Make sure it is 24 bit color as our image processing algorithm expects this format
        options.inPreferredConfig = Bitmap.Config.ARGB_8888

        bitmapOrig = BitmapFactory.decodeResource(resources, R.drawable.termica,options)
        imgOriginal.setImageBitmap(bitmapOrig)

        btnConvertir.setOnClickListener {
            //bitmapWip = Bitmap.createBitmap(bitmapOrig.width, bitmapOrig.height, Bitmap.Config.ALPHA_8)
            //convertToGray(bitmapOrig, bitmapWip)
            //imgNueva.setImageBitmap(bitmapWip)

            /*val source = Mat()
            val optic_cup = Mat()
            val optic_disc = Mat()
            Utils.bitmapToMat(bitmapOrig, source)

            val sample1 = Mat()
            source.copyTo(sample1)

            val sample2 = Mat()
            source.copyTo(sample2)

            println("Size of Source: " + source.size())
            println("Type of Source: " + source.type())

            val channels = ArrayList<Mat>(3)
            Core.split(source, channels)
            val Green = channels[1]
            val Red = channels[0]


            val clahe = createCLAHE()
            clahe.setClipLimit(4.0)
            clahe.apply(Red, Red)

            medianBlur(Red, Red, 3)

            val dest_disc_bitmap = Bitmap.createBitmap(bitmapOrig.getWidth(), bitmapOrig.getHeight(), Bitmap.Config.RGB_565)
            val dest_cup_bitmap = Bitmap.createBitmap(bitmapOrig.getWidth(), bitmapOrig.getHeight(), Bitmap.Config.RGB_565)
            val sample_bitmap = Bitmap.createBitmap(bitmapOrig.getWidth(), bitmapOrig.getHeight(), Bitmap.Config.RGB_565)
            val source_bitmap = Bitmap.createBitmap(bitmapOrig.getWidth(), bitmapOrig.getHeight(), Bitmap.Config.RGB_565)


            Utils.matToBitmap(source, source_bitmap)
            imgNueva.setImageBitmap(source_bitmap)

            val red_element = Imgproc.getStructuringElement(MORPH_ELLIPSE, Size(49.0, 49.0))
            morphologyEx(Red, optic_disc, MORPH_CLOSE, red_element)


            val green_element = Imgproc.getStructuringElement(MORPH_ELLIPSE, Size(49.0, 49.0))
            morphologyEx(Green, optic_cup, MORPH_CLOSE, green_element)

            val max_cup = apply_kmeans(optic_cup, 1, 4)
            val max_disc = apply_kmeans(optic_disc, 2, 5)

            val max = Core.minMaxLoc(optic_disc)
            val maxPoint = max.maxLoc

            val cup_radius = fit_contours(optic_cup, max_cup, maxPoint, sample1)
            val disc_radius = fit_contours(optic_disc, max_disc, maxPoint, sample2)

            Toast.makeText(this@MainActivity, "Radius of Cup: $cup_radius", Toast.LENGTH_LONG).show()
            Toast.makeText(this@MainActivity, "Radius of Disk: $disc_radius", Toast.LENGTH_LONG).show()


            //Utils.matToBitmap(sample1, dest_cup_bitmap)
            //imgNueva.setImageBitmap(dest_cup_bitmap)

            Utils.matToBitmap(sample2, dest_disc_bitmap)*/


            val mat = Mat(bitmapOrig.width, bitmapOrig.height, CvType.CV_8UC3)
            Utils.bitmapToMat(bitmapOrig, mat)

            val rgbMat = Mat()
            cvtColor(mat, rgbMat, COLOR_RGBA2BGR)

            val dilatedMat = Mat()
            val kernel = getStructuringElement(MORPH_ELLIPSE, Size(49.0, 49.0))
            morphologyEx(rgbMat, dilatedMat, MORPH_OPEN, kernel)

            //red
            val redMat = Mat()
            Core.inRange(rgbMat, Scalar(0.0, 0.0, 120.0), Scalar(100.0, 100.0, 255.0), redMat)

            //find contour
            val hierarchy = Mat()
            val contours = ArrayList<MatOfPoint>()

            findContours(redMat, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE)

            var largest_area = 0.0
            var largest_contour_index = 0

            var area = 0
            var rojo = 0

            for (contourIdx in contours.indices) {
                val contourArea = contourArea(contours[contourIdx])

                if (contourArea > largest_area) {
                    largest_area = contourArea
                    largest_contour_index = contourIdx
                    area += 1
                    rojo += 1
                    Log.wtf("areaaa","Largest Area : $largest_area")
                }

                //
            }

            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING

            val mmm  = (area/rojo)*100

            Log.wtf("areaaa","Rojo : $rojo")
            Log.wtf("areaaa","Area : $area")
            Log.wtf("areaaa","Granulation : ${df.format(mmm)} %")

            drawContours(mat, contours, largest_contour_index, Scalar(0.0, 255.0,255.0,255.0), 3)

            val outputImage = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(mat, outputImage)



            imgSobel.setImageBitmap(outputImage)



        }

        btnSobel.setOnClickListener {

            bitmapGray = Bitmap.createBitmap(bitmapOrig.width, bitmapOrig.height, Bitmap.Config.ALPHA_8)
            bitmapWip = Bitmap.createBitmap(bitmapOrig.width, bitmapOrig.height, Bitmap.Config.ALPHA_8)
            // before finding edges, we need to convert this image to gray
            convertToGray(bitmapOrig, bitmapGray)
            // find edges in the image
            findEdges(bitmapGray, bitmapWip)

            imgSobel.setImageBitmap(bitmapWip)

        }

    }

    protected fun apply_kmeans(`object`: Mat, `val`: Int, number: Int): Double {

        val sample = `object`.reshape(1, `object`.cols() * `object`.rows())
        val sample32f = Mat()

        sample.convertTo(sample32f, CvType.CV_32F, 1.0 / 255.0)

        val labels = Mat()
        val criteria = TermCriteria(TermCriteria.COUNT + TermCriteria.EPS, 10000, 0.001)

        val centers = Mat()

        Core.kmeans(sample32f, number, labels, criteria, 2, Core.KMEANS_RANDOM_CENTERS, centers)

        println("Center Size: " + centers.size())
        centers.convertTo(centers, CvType.CV_8UC1, 255.0)
        centers.reshape(number)


        println("Centers: " + centers.dump())

        val r = Core.minMaxLoc(centers)
        var max = r.maxVal

        if (`val` == 2) {
            var maxTwo = 0.0
            for (i in 0 until centers.rows()) {
                val n = centers.get(i, 0)
                println("n value: " + n[0])
                if (max < n[0]) {
                    maxTwo = max
                    max = n[0]

                } else if (maxTwo < n[0] && n[0] != max) {
                    maxTwo = n[0]
                }
            }
            max = (maxTwo + max) / 2
        }
        println("Max value: $max")
        return max
    }

    protected fun fit_contours(`object`: Mat, thresh: Double, maxPoint: Point, source: Mat): Int {

        var maxRadius = 0

        val binary_object = Mat()
        Imgproc.threshold(`object`, binary_object, thresh, 255.0, Imgproc.THRESH_BINARY)

        val contours = ArrayList<MatOfPoint>()
        Imgproc.findContours(binary_object, contours, Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE)
        val maxArea = 0.0
        val intensity = DoubleArray(`object`.total().toInt() * `object`.channels())
        val radius = FloatArray(1)
        val center = Point()

        var min_distance = 9999.0
        var distance: Double
        var required_center = Point()

        for (i in contours.indices) {
            val c = contours[i]
            val c2f = MatOfPoint2f(*c.toArray())
            Imgproc.minEnclosingCircle(c2f, center, radius)

            println("Max Point x: " + maxPoint.x)
            println("Max Point y: " + maxPoint.y)
            println("Center Point x: " + center.x)
            println("Center Point y: " + center.y)

            distance = Math.sqrt(Math.pow(center.x - maxPoint.x, 2.0) + Math.pow(center.y - maxPoint.y, 2.0))

            println("Distance is: $distance")

            if (min_distance > distance) {
                maxRadius = radius[0].toInt()
                required_center = center.clone()
                min_distance = distance
                println("Minimum Distance is: $distance")
            }
        }

        println("Required Radius: $maxRadius")
        println("Final Center is: x=" + required_center.x + " y=" + required_center.y)

        circle(source, required_center, maxRadius, Scalar(0.0, 255.0, 0.0), 15)

        return maxRadius
    }

}
